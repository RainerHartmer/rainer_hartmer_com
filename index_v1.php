<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale1=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">       <!-- (1) -->

   <meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">

   <link rel="apple-touch-icon" sizes="57x57" href="favicon_rainer/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon_rainer/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon_rainer/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon_rainer/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon_rainer/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon_rainer/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon_rainer/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon_rainer/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_rainer/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon_rainer/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon_rainer/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon_rainer/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon_rainer/favicon-16x16.png">
    <link rel="manifest" href="favicon_rainer/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicon_rainer/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

   <link rel="stylesheet"
   href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">                        <!-- (2) -->

   <link href="css/all.css" rel="stylesheet" type="text/css">  <!-- (3) -->


   <title>Rainer Hartmer - web showroom</title>
</head>

<body>
   <header class="site-header">
        <div class="wrapper">
            <h1 class="logoheader"><img src="img/logo.gif"></h1>
            <p>Rainer Hartmer<br>
                <span class="slogan">graphic design &amp; more</span></p>
        </div>
    </header>

    <nav class="mainav">
        <div class="wrapper">
        <button class="menubutton" onclick="this.classList.toggle('show-menu')"><i class="fas fa-bars"></i> &nbsp; navigation</button>
        <ul class="sitenavlist">
            <li><a href="#top">news</a></li>
            <li><a href="#">interior</a></li>
            <li><a href="#">fotografitty</a></li>
            <li><a href="#">sketchbook</a></li>
            <li><a href="sites/ghedhee.html">video &amp; more</a></li>
            <li><a href="#">contact</a></li>
        </ul>
        </div>
    </nav>
<main>

<section id="slider">
   <div class="w3-content w3-section">
    <img class="mySlides" src="img/concrete_small.jpg">
    <img class="mySlides" src="img/lips_small.jpg">
    <img class="mySlides" src="img/loewenzahn_small.jpg">
    <img class="mySlides" src="img/pearls_small.jpg">
    <img class="mySlides" src="img/redr_small.jpg">
  </div>
<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}
  x[myIndex-1].style.display = "block";
  setTimeout(carousel, 3000); // Change image every 2 seconds
}
</script>
</section>
      <section id="html">
         <div class="wrapper">
          <article>
              <header>
                  <h2>Bausteinprüfung Webprogrammierung</h2>
              </header>
              <br>
              <p>Danke für's Vorbeischauen,<br><br>Hier ist leider noch Baustelle, aber der Video Bereich ist schon sehr weit!<br></p>
                  <p>Alles zu meinen Videos und so findest Du unter dem Reiter oben namens "video &amp; more", oder <a href="sites/ghedhee.html">hier!</a></p>
              <footer>

              </footer>
          </article>
          <article>
               <header>
                  <h2>html</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          <article>
               <header>
                  <h2>html</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          </div>
      </section>
      <section id="css">
         <div class="wrapper">
          <article>
              <header>
                  <h2>css</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          <article>
               <header>
                  <h2>css</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          <article>
               <header>
                  <h2>css</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          </div>
      </section>
      <section id="flex">
          <div class="wrapper">
          <article class="erster">
              <header>
                  <h2>flex</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          <article>
               <header>
                  <h2>flex</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="http://google.de">mehr dazu</a>
              </footer>
          </article>
          <article>
               <header>
                  <h2>flex</h2>
              </header>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis optio iusto voluptate tempore quaerat debitis officiis incidunt consectetur ut aspernatur necessitatibus dolorum, suscipit! Voluptate enim nam dolore necessitatibus illo quidem sint natus officia qui ullam perferendis voluptatibus commodi deserunt provident repellendus distinctio, fugiat inventore? Sequi recusandae rem reprehenderit unde ratione.</p>
              <footer>
                  <a href="#">mehr dazu</a>
              </footer>
          </article>
          </div>
      </section>
    </main>

  <footer id="foot">
    <div class="wrapper">
      <p>&copy; Rainer Hartmer
        <a href="sites/impressum.html"> Impressum</a>
        <a href="#top"> nach oben</a>
      </p>
    </div>
  </footer>

</body>
<!-- --------------------------------------------- -->
<!-- --(1)---------------------------------------- -->
<!-- - content="aussagekräftige Beschreibung der - -->
<!-- - Seite, keine Keywords max. 300 Zeichen" --- -->
<!-- --------------------------------------------- -->
<!-- --------------------------------------------- -->

<!-- --------------------------------------------- -->
<!-- --(2)---------------------------------------- -->
<!-- - Einbindung von Fontawesome ---------------- -->
<!-- --------------------------------------------- -->
<!-- --------------------------------------------- -->

<!-- --------------------------------------------- -->
<!-- --(3)---------------------------------------- -->
<!-- - Einbindung externes CSS ------------------- -->
<!-- --------------------------------------------- -->
<!-- --------------------------------------------- -->

<!-- --------------------------------------------- -->
<!-- --(4)---------------------------------------- -->
<!-- - Einbindung internes CSS ------------------- -->
<!-- --------------------------------------------- -->
<!-- --------------------------------------------- -->
</html>
