<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.9.2
 */

/**
 * Database `archiv_rainer`
 */

/* `archiv_rainer`.`game_list` */
$game_list = array(
  array('me_id' => '1','game_titel' => 'Overwatch','game_publisher' => 'Blizzard Entertainment','game_year' => '2016','game_genre' => 'fps','game_img' => 'logo_overwatch.gif','game_homepage' => 'https://playoverwatch.com/de-de/','game_comment' => 'Overwatch - mein Lieblingsshooter - strategisch, abwechslungsreich und spannend - Keine monatlichen Kosten, ein Mal bezahlt: immer dabei!'),
  array('me_id' => '2','game_titel' => 'World of Warcraft','game_publisher' => 'Blizzard Entertainment','game_year' => '2004','game_genre' => 'mmorpg','game_img' => 'logo_wow.gif','game_homepage' => 'https://worldofwarcraft.com/de-de/','game_comment' => 'WoW - der Klassiker unter den mmorpgs. Regelmässige Erweiterungen locken Millionen von Spielern. Gratis sind leider nur die Inhalte bis lvl 20.'),
  array('me_id' => '3','game_titel' => 'Star Wars the old Republic','game_publisher' => 'BioWare','game_year' => '2011','game_genre' => 'mmorpg','game_img' => 'logo_swtor.gif','game_homepage' => 'http://www.swtor.com/de','game_comment' => 'Swtor ist ein Star Wars basierter Multiplayer, mit fantastischen, vollverfilmten Handlungsbögen, dem besten mmorpg PvP System und einem superflexiblen Bezahlsystem - Bis auf die jeweils aktuelle Erweiterung ist SWTOR kostenlos spielbar.'),
  array('me_id' => '4','game_titel' => 'Runescape','game_publisher' => 'Jagex Games Studio','game_year' => '2001','game_genre' => 'mmorpg','game_img' => 'logo_runescape.gif','game_homepage' => 'https://www.runescape.com/l=1/community','game_comment' => 'Runescape ist zwar ein Dinosaurier unter den mmos, dennoch hält es durchaus Schritt und begeistert auch heute noch sehr viele Spieler weltweit. RS hat einen recht grossen free to play content.'),
  array('me_id' => '5','game_titel' => 'Diablo III','game_publisher' => 'Blizzard Entertainment','game_year' => '2012','game_genre' => 'hack and slay','game_img' => 'logo_diablo.gif','game_homepage' => 'https://eu.diablo3.com/de/','game_comment' => 'Diablo III ist ein spektakulär düsteres Hack and Slay mit herausragenden Stories, Dungeons und Gegnern, die man mit alleine oder mit Freunden erkunden kann. DIABLO III -  das erste Kapitel gibt es gratis mit dem battle.net account.'),
  array('me_id' => '6','game_titel' => 'Heroes of the Storm','game_publisher' => 'Blizzard Entertainment','game_year' => '2015','game_genre' => 'moba','game_img' => 'logo_hots.gif','game_homepage' => 'https://heroesofthestorm.com/de-de/','game_comment' => 'Heroes of the Storm ist ein witziges und spannendes MOBA basierend auf den von Blizzard geschaffenen Welten und Charakteren. HOTS gibt es gratis mit dem battle.net account.'),
  array('me_id' => '7','game_titel' => 'Starcraft II','game_publisher' => 'Blizzard Entertainment','game_year' => '2010','game_genre' => 'Echtzeit Strategie','game_img' => 'logo_sc2.gif','game_homepage' => 'https://starcraft2.com/de-de/','game_comment' => 'Starcraft II ist ein Echtzeit Strategie Spiel, das trotz seines Alters stets neue Spieler und Wettbewerbe findet und nichts an Spannung zu wünschen übrig lässt. SCII gibt es gratis mit dem battle.net account.'),
  array('me_id' => '8','game_titel' => 'Hearthstone','game_publisher' => 'Blizzard Entertainment','game_year' => '2014','game_genre' => 'online trading card game','game_img' => 'logo_hearthstone.gif','game_homepage' => 'https://playhearthstone.com/de-de/','game_comment' => 'Hearthstone ist ein witziges und spannendes online trading card game basierend auf den von Blizzard geschaffenen Welten und Charakteren. HS gibt es gratis mit dem battle.net account.'),
  array('me_id' => '9','game_titel' => 'SQUAD','game_publisher' => 'Offworld Industries','game_year' => '2015','game_genre' => 'fps','game_img' => 'logo_squad.gif','game_homepage' => 'https://store.steampowered.com/app/393380/Squad/','game_comment' => 'Squad ist ein taktischer first person shooter für bis zu 70 vs. 70 Spieler, mit karten von über 15qkm Größe und friendly fire, ein Chaos in das ich mich gerade hineinspiele. SQUAD gibt es gratis auf Steam.'),
  array('me_id' => '10','game_titel' => 'Darwin Project','game_publisher' => 'Scavengers Studio','game_year' => '2018','game_genre' => 'battle royale survival','game_img' => 'logo_darwin.gif','game_homepage' => 'https://store.steampowered.com/app/544920/Darwin_Project/','game_comment' => 'Darwin Project ist ein survival battle royale vom feinsten: Ein Bogen, ein paar Pfeile, eine Axt - das ist die Basisaustattung, damit geht es los. Dichter kommt man aktuell nicht an The Hunger Games heran. DP gibt es gratis auf Steam.'),
  array('me_id' => '11','game_titel' => 'Counter Strike GO','game_publisher' => 'Valve Corporation','game_year' => '2012','game_genre' => 'fps','game_img' => 'logo_csgo.gif','game_homepage' => 'https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/','game_comment' => 'Counter Strike Global Offensive ist ein klassischer first person shooter mit verschiedenen maps und Spielweisen. Ich bin mit dem Spiel erst angefangen, finde es aber sehr kurzweilig, herausfordernd und spannend. CSGO gibt es gratis auf Steam.'),
  array('me_id' => '12','game_titel' => 'Pong','game_publisher' => 'Allan Alcorn','game_year' => '1972','game_genre' => 'virtuelles Tischtennis','game_img' => 'logo_pong.gif','game_homepage' => 'https://www.spielaffe.de/Pong-Spiele','game_comment' => 'Pong war eines von mehreren Spielen, die auf der Konsole waren, die mein Vater irgendwann Anfang der 80er gekauft hat. Gespielt wurde auf einem uralten und riesigen Schwarzweiss Fernseher...')
);

/* `archiv_rainer`.`video_list` */
$video_list = array(
  array('me_id' => '4','video_embed' => 'https://www.youtube-nocookie.com/embed/MQOpXl0Hw0A','video_type' => 'World of Warcraft','video_titel' => 'Teaser'),
  array('me_id' => '5','video_embed' => 'https://www.youtube-nocookie.com/embed/hpcis7pP8XQ','video_type' => 'Overwatch','video_titel' => 'Flankhardt'),
  array('me_id' => '6','video_embed' => 'https://www.youtube-nocookie.com/embed/STyDkiHJfW8','video_type' => 'Overwatch','video_titel' => 'Winter is coming'),
  array('me_id' => '7','video_embed' => 'https://www.youtube-nocookie.com/embed/wQBJBQQfh8w','video_type' => 'Overwatch','video_titel' => 'Stream'),
  array('me_id' => '8','video_embed' => 'https://www.youtube-nocookie.com/embed/7WEhVHOjhcE','video_type' => 'Overwatch','video_titel' => 'Full Stream'),
  array('me_id' => '9','video_embed' => 'https://www.youtube-nocookie.com/embed/zMm2dAcs6e8','video_type' => 'World of Warcraft','video_titel' => 'Blackmoore'),
  array('me_id' => '10','video_embed' => 'https://www.youtube-nocookie.com/embed/AEJvn3ZwKfQ','video_type' => 'World of Warcraft','video_titel' => 'Alterac Teaser'),
  array('me_id' => '11','video_embed' => 'https://www.youtube-nocookie.com/embed/jIebZJMZt9w','video_type' => 'World of Warcraft','video_titel' => 'Alterac Valley'),
  array('me_id' => '12','video_embed' => 'https://www.youtube-nocookie.com/embed/1X7tu_9CZBo','video_type' => 'World of Warcraft','video_titel' => 'Sungod'),
  array('me_id' => '13','video_embed' => 'https://www.youtube-nocookie.com/embed/2FSoBXYhfMM','video_type' => 'World of Warcraft','video_titel' => 'Sungod full match'),
  array('me_id' => '14','video_embed' => 'https://www.youtube-nocookie.com/embed/3uwAg8w5u5k','video_type' => 'World of Warcraft','video_titel' => 'Why me?'),
  array('me_id' => '15','video_embed' => 'https://www.youtube-nocookie.com/embed/FkQy6rs_Tdg','video_type' => 'World of Warcraft','video_titel' => 'You don\'t own me'),
  array('me_id' => '16','video_embed' => 'https://www.youtube-nocookie.com/embed/k_hKIy3SUog','video_type' => 'World of Warcraft','video_titel' => 'friday evening'),
  array('me_id' => '17','video_embed' => 'https://www.youtube-nocookie.com/embed/H0rcu0B6FmU','video_type' => 'World of Warcraft','video_titel' => 'saturday afternoon'),
  array('me_id' => '18','video_embed' => 'https://www.youtube-nocookie.com/embed/4DyNNyWILdM','video_type' => 'World of Warcraft','video_titel' => 'saturday evening'),
  array('me_id' => '19','video_embed' => 'https://www.youtube-nocookie.com/embed/Y1Gf9e7n_Ps','video_type' => 'Darwin Project','video_titel' => 'cut stream'),
  array('me_id' => '20','video_embed' => 'https://www.youtube-nocookie.com/embed/TswTWNNRLTc','video_type' => 'Darwin Project','video_titel' => 'full stream')
);
