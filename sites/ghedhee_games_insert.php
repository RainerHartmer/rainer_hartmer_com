<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale1=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">              <!-- (1) -->

   <meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">

    <link rel="apple-touch-icon" sizes="57x57" href="../favicon_ghedhee/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../favicon_ghedhee/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../favicon_ghedhee/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../favicon_ghedhee/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../favicon_ghedhee/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../favicon_ghedhee/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../favicon_ghedhee/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../favicon_ghedhee/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../favicon_ghedhee/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../favicon_ghedhee/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon_ghedhee/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../favicon_ghedhee/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon_ghedhee/favicon-16x16.png">
    <link rel="manifest" href="../favicon_ghedhee/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../favicon_ghedhee/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
                                                                      <!-- (2) -->
   <link rel="stylesheet"
   href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">                                        <!-- (3) -->
   <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">

   <link href="../css/ghedhee.css" rel="stylesheet" type="text/css">     <!-- (4) -->

   <title lang="en">Ghedhee - @ll games</title>

</head>

<body>
   <header class="site-header">
        <div class="wrapper">
            <h1 class="logoheader"><img src="../img/ghedhee.gif"></h1>
            <p lang="en">Ghedhee<br>
                <span class="slogan" lang="en">video editing &amp; gaming</span></p>
        </div>
    </header>

    <nav class="mainav">
      <div class="wrapper">
        <button class="menubutton" onclick="this.classList.toggle('show-menu')"><i class="fas fa-bars"></i> &nbsp; Navigation</button>
        <ul class="sitenavlist">
            <li><a href="ghedhee.html">video</a></li>
            <li><a href="ghedhee_gaming.html">gaming</a></li>
            <li><a href="ghedhee_videos.php">@ll videos</a></li>
            <li><a href="ghedhee_games.php">@ll games</a></li>
            <li><a href="../index.php">design &amp; more</a></li>
            <li><a href="ghedhee_impressum.html">contact</a></li>
        </ul>
      </div>
    </nav>

 <section id="test">
 <div class="wrapper">
 <div id="videoheader">
  <video autoplay="autoplay" muted="muted" loop="loop">
    <source src="../video/fire_background_loop2_videvo2.mov" type="video/mp4">
  </video>
  </div>
  </div>
  </section>

<main>
  <section id="gamelist">
    <div class="wrapper">
      <article>

    <?php

        $titel = $_POST["titel"];
        $jahr = $_POST["jahr"];
        $publisher = $_POST["publisher"];
        $genre = $_POST["genre"];
        $beschreibung = $_POST["beschreibung"];
        $homepage = $_POST["homepage"];
        $cover = $_FILES["cover"]["name"];
        $hilfscover = $_FILES["cover"]["tmp_name"];

 /* Schritt 1 und 2 Verbindung herstellen und DB auswählen */
     $mysqli = new mysqli('localhost','root','','archiv_rainer');

     if($mysqli->connect_error) {
     echo 'Fehler bei der Verbindung: ' . mysqli_connect_error() . '<br>';
     die();//exit();
     }
     /* Zeichensatz UTF8 einstellen */
     if(!$mysqli->set_charset('utf8')) {
     echo 'Fehler beim Laden von UTF8 ' . $mysqli->error;
     }
    //  echo 'Verbindung hergestellt<br>';

//Verbindung zur DB

//SQL-Abfrage
$tabelle = "game_list";
$einfuegen = "INSERT INTO $tabelle (me_id, game_titel, game_publisher, game_year, game_genre, game_img, game_homepage, game_comment) VALUES ('','$titel','$publisher',$jahr,'$genre','$cover','$homepage','$beschreibung')";

// mysqli_query($datenbank, $einfuegen) or die("Fehler: ".
// mysqli_error());

$mysqli->query($einfuegen) or die("Fehler bitte was eingeben: ".mysqli_error($mysqli));
//Datei hochladen
move_uploaded_file($hilfscover, "../img/".$cover);
echo "Neue DVD eingefügt!<br>";
echo  '<ul>'
 . '<li>Titel: ' . $titel . '</li>'
 . '<li>Cover: ' . $cover . '</li>'
 . '<li>Erscheinungsjahr: ' . $jahr . '</li>'
 . '<li>Publisher: ' . $publisher . '</li>'
 . '<li>Genre: ' . $genre . '</li>'
 . '<li>homepage: ' . $homepage . '</li>'
 . '<li>Beschreibung: ' . $beschreibung . '</li>';

echo '</ul>';


  /* Schritt 5 Verbindung schließen */
  $mysqli->close();

      ?>

  <!--  </div>
  </section>-->
      </article>
      </div>
    </section>
</main>

  <footer id="foot">
    <div class="wrapper">
      <p>&copy; Rainer Hartmer
        <a href="sites/impressum.html">Impressum</a>
        <a href="#top">nach oben</a>
      </p>
    </div>
  </footer>

</body>
<!-- --------------------------------------------- -->
<!-- - Fußnoten ---------------------------------- -->
<!-- --(1)---------------------------------------- -->
<!-- - content="aussagekräftige Beschreibung der - -->
<!-- - Seite, keine Keywords max. 300 Zeichen" --- -->
<!-- --------------------------------------------- -->
<!-- --(2)---------------------------------------- -->
<!-- - Einbindung von w3 schools css ------------- -->
<!-- --------------------------------------------- -->
<!-- --(3)---------------------------------------- -->
<!-- - Einbindung fontawesome ------------------- -->
<!-- --------------------------------------------- -->
<!-- --(4)---------------------------------------- -->
<!-- -- Einbindung externes CSS ------------------ -->
<!-- --------------------------------------------- -->
</html>
