<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale1=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">              <!-- (1) -->

   <meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">
   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
                                                                      <!-- (2) -->
   <link rel="stylesheet"
   href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">                                        <!-- (3) -->
   <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">

   <link href="../css/ghedhee.css" rel="stylesheet" type="text/css">     <!-- (4) -->

   <title lang="en">Ghedhee - @ll games</title>

</head>

<body>
   <header class="site-header">
        <div class="wrapper">
            <h1 class="logoheader"><img src="../img/ghedhee.gif"></h1>
            <p lang="en">Ghedhee<br>
                <span class="slogan" lang="en">video editing &amp; gaming</span></p>
        </div>
    </header>

    <nav class="mainav">
      <div class="wrapper">
        <button class="menubutton" onclick="this.classList.toggle('show-menu')"><i class="fas fa-bars"></i> &nbsp; Navigation</button>
        <ul class="sitenavlist">
            <li><a href="ghedhee.html">video</a></li>
            <li><a href="ghedhee_gaming.html">gaming</a></li>
            <li><a href="ghedhee_videos.php">@ll videos</a></li>
            <li><a href="ghedhee_games.php">@ll games</a></li>
            <li><a href="../index.php">design &amp; more</a></li>
            <li><a href="ghedhee_impressum.html">contact</a></li>
        </ul>
      </div>
    </nav>

 <section id="test">
 <div class="wrapper">
 <div id="videoheader">
  <video autoplay="autoplay" muted="muted" loop="loop">
    <source src="../video/fire_background_loop2_videvo2.mov" type="video/mp4">
  </video>
  </div>
  </div>
  </section>

<main>
<section>
  <div class="wrapper">
<?php
    /* https://www.php.net/manual/de/index.php */


    /* Schritt 1 und 2 Verbindung herstellen und DB auswählen */
    $mysqli = new mysqli('localhost','root','','game_archiv');

    if($mysqli->connect_error) {
    echo 'Fehler bei der Verbindung: ' . mysqli_connect_error() . '<br>';
    die();//exit();
    }
    /* Zeichensatz UTF8 einstellen */
    if(!$mysqli->set_charset('utf8')) {
    echo 'Fehler beim Laden von UTF8 ' . $mysqli->error;
    }
    // echo 'Verbindung hergestellt<br>';

    /* Schritt 3 Abfrage durchführen */
    $ergebnis = $mysqli->query('SELECT * FROM meine_dvd;');

    /* Schritt 4 Ergebnis für Ausgabe aufbereiten.
    fetch_array() gibt die Daten als Array zurück. */


 //Datensatz löschen
 if (isset($_POST["loeschen"])){
    $loeschen = $_POST["loeschen"];
    $id = $_POST["id"];
    $tabelle = "meine_dvd";
    $loesche = "DELETE FROM $tabelle WHERE dvd_id = $id";
    $mysqli->query($loesche) or die("Fehler: ".mysqli_error());

    echo '<div class="container text-danger">Eintrag gelöscht</div>';
    // $cMeldung = 'Eintrag wurde gelöscht';
    // echo '<script>alert("'.$cMeldung.'");</script>';
}


    echo '<div class="container">'
        .'<h2 class="text-muted">DVD löschen</h2>'
        .'<div class="row">';

    while($zeile = $ergebnis->fetch_array()) {

        echo '<article class="col-md-4 py-5">'
        .'<h2>' . htmlspecialchars($zeile['dvd_titel']) .'</h2>'
        . '<p class="lead">' . htmlspecialchars($zeile['dvd_genre']) .'</p>'
        . '<p class="text-muted">Erscheinungsjahr: '. htmlspecialchars($zeile['dvd_jahr']) .'<br>'
        . 'Dauer: '. htmlspecialchars($zeile['dvd_min']) .' Minuten <br>'
        . 'FSK: '. htmlspecialchars($zeile['dvd_fsk']) .'</p>'
        . '<img src="img/' . htmlspecialchars($zeile['dvd_cover']) . '" title="' . htmlspecialchars($zeile['dvd_titel']) . '" alt="' . htmlspecialchars($zeile['dvd_titel']) . '">'
        . '<p>' . htmlspecialchars($zeile['beschreibung']) . '</p>'
        .'<form method="POST"><input type="hidden" name="id" value="'. htmlspecialchars($zeile['dvd_id']) .'">'
        . '<input type="submit" name="loeschen" value="Löschen"></form>'
        . '</article>';
    }


    echo '</div></div>';

    /* Schritt 5 Verbindung schließen */
    $mysqli->close();
    // header('Location: '.$_SERVER['REQUEST_URI']);
    ?>
      </div>
    </section>

</main>

  <footer id="foot">
    <div class="wrapper">
      <p>&copy; Rainer Hartmer
        <a href="sites/impressum.html">Impressum</a>
        <a href="#top">nach oben</a>
      </p>
    </div>
  </footer>

</body>
<!-- --------------------------------------------- -->
<!-- - Fußnoten ---------------------------------- -->
<!-- --(1)---------------------------------------- -->
<!-- - content="aussagekräftige Beschreibung der - -->
<!-- - Seite, keine Keywords max. 300 Zeichen" --- -->
<!-- --------------------------------------------- -->
<!-- --(2)---------------------------------------- -->
<!-- - Einbindung von w3 schools css ------------- -->
<!-- --------------------------------------------- -->
<!-- --(3)---------------------------------------- -->
<!-- - Einbindung fontawesome ------------------- -->
<!-- --------------------------------------------- -->
<!-- --(4)---------------------------------------- -->
<!-- -- Einbindung externes CSS ------------------ -->
<!-- --------------------------------------------- -->
</html>
